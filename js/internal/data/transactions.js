var MyManager = MyManager || {};
var MyManagerTransactions = MyManagerTransactions || {};
var transactionList=[];

MyManagerTransactions.fetchAllTransactions = function() {
	var responseText = '{"transactions":[{"id":"23","date":"23/08/2016","time":"20:26","type":"Expense","subtype":"Bill","isCredit":"true","desc":"Electricity Bill for July 2016","amount":"2403.29"},{"id":"22","date":"23/06/2016","time":"14:59","type":"Income","subtype":"Debt","isCredit":"false","desc":"Money Transfered by Emon","amount":"607.00"},{"id":"21","date":"23/08/2016","time":"07:27","type":"Expense","subtype":"Travel","isCredit":"true","desc":"Trip at mandarmani with friends","amount":"5077.93"},{"id":"20","date":"19/08/2016","time":"08:38","type":"Expense","subtype":"Bill","isCredit":"true","desc":"Mobile Recharge for Airtel","amount":"191.00"},{"id":"19","date":"11/08/2016","time":"13:29","type":"Income","subtype":"Salary","isCredit":"false","desc":"Salary for the month of July 2016","amount":"72653.57"},{"id":"18","date":"05/08/2016","time":"10:30","type":"Expense","subtype":"Installment","isCredit":"false","desc":"AC installment for July","amount":"3375.50"},{"id":"17","date":"03/08/2016","time":"12:31","type":"Expense","subtype":"Entertainment","isCredit":"true","desc":"Booked Movie Tickets at Bookmyshow.com of Rustom for Sharmi, Sujoy, Anko and me","amount":"673.85"},{"id":"16","date":"24/07/2016","time":"00:32","type":"Expense","subtype":"Food","isCredit":"true","desc":"Dinner at Oh! Calcutta","amount":"2185.48"},{"id":"15","date":"21/07/2016","time":"18:26","type":"Expense","subtype":"Bill","isCredit":"true","desc":"Electricity Bill for July 2016","amount":"2903.90"},{"id":"14","date":"15/07/2016","time":"16:33","type":"Expense","subtype":"Cash","isCredit":"false","desc":"Withdraw Cash from ATM","amount":"8500.00"},{"id":"13","date":"11/07/2016","time":"14:34","type":"Income","subtype":"Salary","isCredit":"false","desc":"Salary for the month of June 2016","amount":"92895.91"},{"id":"12","date":"07/07/2016","time":"15:35","type":"Income","subtype":"Interest","isCredit":"false","desc":"Interest added on balance","amount":"732.87"},{"id":"11","date":"01/07/2016","time":"16:36","type":"Expense","subtype":"Entertainment","isCredit":"false","desc":"Bought a new Mobile","amount":"18830.09"},{"id":"10","date":"29/06/2016","time":"19:37","type":"Expense","subtype":"Travel","isCredit":"true","desc":"Returned home in Uber","amount":"355.95"},{"id":"9","date":"21/06/2016","time":"09:38","type":"Expense","subtype":"Cash","isCredit":"false","desc":"Withdraw Cash from ATM","amount":"1500.00"},{"id":"8","date":"20/06/2016","time":"22:57","type":"Expense","subtype":"Bill","isCredit":"true","desc":"Electricity Bill for May 2016","amount":"1218.00"},{"id":"7","date":"18/06/2016","time":"20:40","type":"Income","subtype":"Debt","isCredit":"false","desc":"Money Transfered by Mohan","amount":"966.30"},{"id":"6","date":"15/06/2016","time":"19:43","type":"Expense","subtype":"Cash","isCredit":"false","desc":"Withdraw Cash from ATM","amount":"900.00"},{"id":"5","date":"11/06/2016","time":"13:59","type":"Income","subtype":"Salary","isCredit":"false","desc":"Salary for the month of May 2016","amount":"61086.58"},{"id":"4","date":"09/06/2016","time":"22:42","type":"Expense","subtype":"Bill","isCredit":"true","desc":"2 Mobile Recharges for airtel","amount":"507.00"},{"id":"3","date":"11/06/2016","time":"13:59","type":"Income","subtype":"Dividend","isCredit":"false","desc":"Maturity of a Fixed Deposit","amount":"106086.58"},{"id":"2","date":"29/05/2016","time":"17:27","type":"Expense","subtype":"Travel","isCredit":"false","desc":"Flight tickets to Bangalore return","amount":"9355.95"},{"id":"1","date":"27/06/2016","time":"13:29","type":"Income","subtype":"Debt","isCredit":"false","desc":"Money Transfered by Anjum","amount":"1233.90"}]}';
	MyManagerTransactions.setTransactionList(responseText);
};

MyManagerTransactions.setTransactionList = function(responseText) {
var response = JSON.parse(responseText);
transactionList = response.transactions;
};

MyManagerTransactions.getTransactions = function(noOfTransactionShown, loadMoreQty) {
	var start=0;
	var limit=0;
	var displayTransactionList=[];
	if(loadMoreQty!== undefined){
		start = noOfTransactionShown;
		limit = (noOfTransactionShown+loadMoreQty)> transactionList.length ? transactionList.length : (noOfTransactionShown+loadMoreQty);
		for(i= start; i<limit; i++){
			displayTransactionList.push(transactionList[i]);
		}		
	} else {
		start = 0;
		limit = noOfTransactionShown > transactionList.length ? transactionList.length : noOfTransactionShown;
		for(i= start; i<limit; i++){
			displayTransactionList.push(transactionList[i]);
		}
	}
	return displayTransactionList;
};

var getTypedTransaction = function(transactionList, transactionType) {
	var aTransactionList = [];
	$.each(transactionList, function(indx,aTransaction) {
		if(transactionType === aTransaction.type) {
			aTransactionList.push(aTransaction);
		}
	});
	return aTransactionList;
};

MyManagerTransactions.getTransactionByTypes = function(noOfTransactionShown, transactionType, loadMoreQty) {
	var start=0;
	var limit=0;
	var transactionTypedList=[];
	var displayTransactionList=[];
	transactionTypedList = getTypedTransaction(transactionList, transactionType);
	if(loadMoreQty!== undefined){
		start = noOfTransactionShown;
		limit = (noOfTransactionShown+loadMoreQty)> transactionTypedList.length ? transactionTypedList.length : (noOfTransactionShown+loadMoreQty);
		for(i= start; i<limit; i++){
			displayTransactionList.push(transactionTypedList[i]);
		}		
	} else {
		start = 0;
		limit = noOfTransactionShown > transactionTypedList.length ? transactionTypedList.length : noOfTransactionShown;
		for(i= start; i<limit; i++){
			displayTransactionList.push(transactionTypedList[i]);
		}
	}
	return displayTransactionList;
};

MyManagerTransactions.onClickTransactionCreateButton = function() {
	var counter = 0;0
	var errorMessage="";
	var transactionDate = $('#transactionDate');
	var transactionTime = $('#transactionTime');
	var transactionDescription = $('#transactionDescription');
	var transactionAmount = $('#transactionAmount');
	var transactionType = $('#transactionType');
	var transactionSubType = $('#transactionSubType');
	var isTransactionCreditType = $('#isTransactionCreditType');
		
	if (transactionDate.val().isEmpty()) {
		transactionDate.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Transaction date is blank.</li>";
	} else {
		transactionDate.parents(".form-group").removeClass("has-error");
		MyManager.transactionObject['transactionDate'] = transactionDate.val();
		counter++;
	}
	
	if (transactionTime.val().isEmpty()) {
		transactionTime.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Transaction time is blank.</li>";
	} else if (moment(transactionDate.val()+" "+transactionTime.val(), MyManager.dateDisplayFormat).isAfter(moment())) {
		transactionTime.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Future time is not allowed.</li>";
	} else {
		transactionTime.parents(".form-group").removeClass("has-error");
		MyManager.transactionObject['transactionTime'] = transactionTime.val();
		counter++;
	}
	
	if (transactionDescription.val().isEmpty()) {
		transactionDescription.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Description is blank.</li>";
	} else if (transactionDescription.val().length > 250) {
		transactionDescription.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Keep your Description within 250 words.</li>";
	} else {
		transactionDescription.parents(".form-group").removeClass("has-error");
		MyManager.transactionObject['transactionDescription'] = transactionDescription.val();
		counter++;
	}
	
	if (transactionAmount.val().isEmpty()) {
		transactionAmount.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Transaction Amount is blank.</li>";
	} else if(!MyManager.isValidNumberWithDecimal(transactionAmount.val())) {
		transactionAmount.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Only numbers upto 2 decimals are allowed.</li>";
	}else {
		transactionAmount.parents(".form-group").removeClass("has-error");
		MyManager.transactionObject['transactionAmount'] = transactionAmount.val();
		counter++;
	}
	
	if(4 === counter){
		MyManager.transactionObject['transactionType'] = transactionType.val();
		MyManager.transactionObject['transactionSubType'] = transactionSubType.val();
		MyManager.transactionObject['isTransactionCreditType'] = isTransactionCreditType.val();
		return errorMessage;
	} else {
		return errorMessage;
	}
};

MyManagerTransactions.saveData = function(payLoad) {
	console.log(JSON.stringify(payLoad));
	/*$.ajax({
		type : "POST",
		url : "",
		data : JSON.stringify(payLoad),
		contentType : 'application/json',
		dataType : 'json',
		complete : function(data) {
			if (data.responseText === 'failed')
			else {
				MyManagerTransactions.setTransactionList(data.responseText);
				MyManagerTransactions.transactionSetup();*/
			/*}
		}
	});*/
};

