var MyManager = MyManager || {};

$(document).on("click", '#openAccount', function(e) {
	e.preventDefault();
	var counter = 0;
	var alertContainer = $('#step1Alert');
	var errorMessage = "<strong>Errors present:</strong> <ul>";

	var userFirstName = $('#userFirstName');
	var userLastName = $('#userLastName');
	var mobile = $('#mobile');
	var emailId = $('#emailId');
	var password = $('#password');
	var confirmPassword = $('#confirmPassword');

	if ($.trim(userFirstName.val()).length === 0 || $.trim(userLastName.val()).length === 0) {
		userFirstName.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Firstname or Lastname is blank.</li>";
	} else {
		userFirstName.parents(".form-group").removeClass("has-error");
		MyManager.registrationObject['userFirstName'] = userFirstName.val();
		MyManager.registrationObject['userLastName'] = userLastName.val();
		counter++;
	}

	if ($.trim(mobile.val()).length === 0) {
		mobile.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Mobile number cannot be blank.</li>";
	} else if (!MyManager.isValidNumber(mobile.val(),10)) {
		mobile.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Invalid Mobile number.</li>";
	} else {
		mobile.parents(".form-group").removeClass("has-error");
		MyManager.registrationObject['mobile'] = mobile.val();
		counter++;
	}

	if ($.trim(emailId.val()).length === 0) {
		emailId.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Email Id is blank.</li>";
	} else if (!MyManager.isValidEmail(emailId.val())) {
		emailId.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Email Id is incorrect.</li>";
	} else {
		emailId.parents(".form-group").removeClass("has-error");
		MyManager.registrationObject['emailId'] = emailId.val();
		counter++;
	}

	if ($.trim(password.val()).length === 0 || $.trim(confirmPassword.val()).length === 0) {
		password.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Password or Confirm Password is blank.</li>";
	} else if (!MyManager.isPasswordFormatMatch(password.val())) {
		password.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Password does not match the required criteria.</li>";
	} else if (password.val() !== confirmPassword.val()) {
		password.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Passwords does not match.</li>";
	} else {
		password.parents(".form-group").removeClass("has-error");
		MyManager.registrationObject['password'] = password.val();
		counter++;
	}

	flag = (4 === counter);
	if (flag) {
		MyManager.registrationObject['regTimeStamp'] = moment.utc().format(MyManager.timeFormat);
		flag = MyManager.registerUser(MyManager.registrationObject);
		if (!flag)
			errorMessage = errorMessage + "<li>Data could not be saved. Server Error occurred.</li>";
	}

	if (flag) {
		MyManager.showAlertInBox(alertContainer, false);
		$('#step1Tab').data('value', 1);
		$('#step2Tab').attr('href', "#planSelection").removeClass('disabled');
		$(this).prop('href', '#planSelection');
		$(this).addClass("disabled");
		$(this).parents('form').find('input').attr('disabled', true);
		$(this).tab('show');
	} else {
		errorMessage = errorMessage + "</ul>";
		MyManager.showAlertInBox(alertContainer, true, errorMessage, 'error');
	}
});

$(document).on("click", '#finalizeButton', function(e) {
	e.preventDefault();
	var counter = 0;
	var alertContainer = $('#step3Alert');
	var errorMessage = "<strong>Errors present:</strong> <ul>";

	var age = $('#age');
	var locality = $('#locality');
	var aptno = $('#aptno');
	var street = $('#street');
	var landmark = $('#landmark');
	var state = $('#state');
	var pin = $('#pin');
	var country = $('#country');
	var city = $('#city');
	MyManager.finalizeObject['accountNo'] = $('#accountInfo2').text();
	MyManager.finalizeObject['plan'] = $('#finalPlan').data('value');
	MyManager.finalizeObject['planDescription'] = $('#finalPlan').text();
	MyManager.finalizeObject['price'] = $('#finalPlan').data('price');
	MyManager.finalizeObject['term'] = $('#finalPlan').data('term');
	MyManager.finalizeObject['discount'] = MyManager.getDiscount(MyManager.finalizeObject.term, MyManager.finalizeObject.plan);
	MyManager.finalizeObject['regStartDate'] = moment.utc().format(MyManager.dateFormat);
	MyManager.finalizeObject['regEndDate'] = moment.utc().add(MyManager.finalizeObject.term, 'M').format(MyManager.dateFormat);
	MyManager.finalizeObject['selectCurrency'] = $('#selectCurrency').find("option:selected").val();
	MyManager.finalizeObject['selectCurrencyDescription'] = $('#selectCurrency').find("option:selected").attr("title");
	if (12 >= age.text()) {
		age.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Age must be above 12 years.</li>";
	} else {
		age.parents(".form-group").removeClass("has-error");
		MyManager.finalizeObject['dateOfBirth'] = $('#dateOfBirth').val();
		MyManager.finalizeObject['age'] = age.text();
		counter++;// 1
	}
	if ($.trim(country.val()).length === 0 || $.trim(city.val()).length === 0) {
		country.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Location or City cannot be blank.</li>";
	} else {
		country.parents(".form-group").removeClass("has-error");
		MyManager.finalizeObject['country'] = country.val();
		MyManager.finalizeObject['city'] = city.val();
		counter++;// 2
	}
	if ($.trim(locality.val()).length === 0 || $.trim(aptno.val()).length === 0 && $.trim(street.val()).length === 0) {
		locality.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> Locality and Address cannot be blank.</li>";
	} else {
		locality.parents(".form-group").removeClass("has-error");
		MyManager.finalizeObject['locality'] = locality.val();
		MyManager.finalizeObject['aptno'] = aptno.val();
		MyManager.finalizeObject['street'] = street.val();
		counter++;// 3
	}
	if ($.trim(state.val()).length === 0 || $.trim(pin.val()).length === 0) {
		landmark.parents(".form-group").addClass("has-error");
		errorMessage = errorMessage + "<li> State and Pin cannot be blank.</li>";
	} else {
		landmark.parents(".form-group").removeClass("has-error");
		MyManager.finalizeObject['landmark'] = landmark.val();
		MyManager.finalizeObject['state'] = state.val();
		MyManager.finalizeObject['pin'] = pin.val();
		counter++;// 4
	}
	MyManager.finalizeObject['uniqueId'] = $('#uniqueId').val(); 
	if (4 === counter) {
		MyManager.showAlertInBox(alertContainer, false);
		MyManager.setupVerifyPage(MyManager.registrationObject, MyManager.finalizeObject)
		$(this).prop('href', '#detailsOfRegistration');
		$(this).tab('show');
	} else {
		errorMessage = errorMessage + "</ul>";
		MyManager.showAlertInBox(alertContainer, true, errorMessage, 'error');
	}
});

MyManager.registerUser = function(payLoad) {
	console.log(JSON.stringify(payLoad));
	/*$.ajax({
		type : "POST",
		url : "",
		data : JSON.stringify(payLoad),
		contentType : 'application/json',
		dataType : 'json',
		complete : function(data) {
			if (data.responseText === 'failed')
				return false;
			else {
				$('#accountInfo1').text(data.responseText);
				$('#accountInfo2').text(data.responseText);
				$('#accountInfo3').text(data.responseText);*/
				return true;
			/*}
		}
	});*/
};

MyManager.saveFinalData = function(payLoad) {
	console.log(JSON.stringify(payLoad));
	/*$.ajax({
		type : "POST",
		url : "",
		data : JSON.stringify(payLoad),
		contentType : 'application/json',
		dataType : 'json',
		complete : function(data) {
			if (data.responseText === 'failed')
				return false;
			else {*/
				return true;
			/*}
		}
	});*/
};