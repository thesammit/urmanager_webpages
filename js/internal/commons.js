var MyManager = MyManager || {};

MyManager.timeFormat = 'DD/MM/YYYY HH:mm:ss Z';
MyManager.inputDateFormat = 'DD/MM/YYYY';
MyManager.inputTimeFormat = 'HH:mm';
MyManager.dateFormat = 'DD/MM/YYYY HH:mm Z';
MyManager.datetimeDisplayFormat = 'DD/MM/YYYY hh:mm:ss A';
MyManager.dateDisplayFormat = 'DD/MM/YYYY HH:mm';
MyManager.singleDateDisplayFormat = 'Do MMM YYYY';
MyManager.singleTimeDisplayFormat = 'hh:mm A';
MyManager.MONTH = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];	
MyManager.FULLMONTH = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
MyManager.emiStatus = {"paid":"0","pending":"1","overdue":"2"};
MyManager.currentMonth = moment().get('month');
MyManager.currentYear = moment().get('year');
MyManager.registrationObject = {};
MyManager.finalizeObject = {};
MyManager.planObject = {};
MyManager.transactionObject = {};
MyManager.VAT = 0.135;
MyManager.CESS = 0.02;
MyManager.SERVICE_TAX = 0.02;
MyManager.colors = {
		"SUCCESS":"#4caf50",
		"DANGER":"#e51c23",
		"INFO":"#9c27b0",
		"WARNING":"#ff9800",
		"PRIMARY":"#2196f3",
		"DEFAULT":"#666666",
		"SKYBLUE":"#04C8D6",
		"YELLOW":"#E8BD00",
		"CHOCOLATE":"#431C00",
		"GREY":"#808080",
		"WHITE":"#FFFFFF"
};
MyManager.expenseGroups = [
                           "Food",
                           "Travel",
                           "Entertainment",
                           "Bill",
                           "Installment",
                           "Cash"
                           ];
MyManager.incomeGroups = [
                           "Salary",
                           "Debt",
                           "Dividend",
                           "Interest",
                           "Cash"
                           ];

MyManager.getColorOfSet = function(colorCode, opacity) {
	var hexColor = colorCode.replace(/#/, '');
	var red = parseInt(hexColor.substring(0, 2), 16);
	var green = parseInt(hexColor.substring(2, 4), 16);
	var blue = parseInt(hexColor.substring(4, 6), 16);

	var result = 'rgba(' + red + ',' + green + ',' + blue + ',' + (opacity || '.3') + ')';
	return result;
};

MyManager.getConfigData = function() {
	var configData = {};
	configData.noOfMonthsOnChart = 9;
	configData.currencyIcon = 'INR';
	configData.creditCardLimit = "119000.00";
	configData.creditCardPointAmount = "";
	configData.creditCardPointFactor = "";
	configData.creditCardPointConvertor = "";
	configData.creditBillingDate = "";
	configData.creditPaymentDate = "";
	configData.bankAccounts = ["02641610012865","50200005069274"];
	return configData;
};

MyManager.randomScalingFactor = function() {
	return Math.round(Math.random() * 100 );
};

MyManager.getStatusIcon = function (statusCode) {
	var icon = '<i class="fa ';
	switch(statusCode) {
	case 0:
		icon = icon + 'fa-check-circle text-success';
		break;
	case 1:
		icon = icon + 'fa-warning text-warning';
		break;
	case 2:
		icon = icon + 'fa-times-circle text-danger';
		break;
	case 3:
		icon = icon + 'fa-info-circle text-primary';
		break;
	}
	icon = icon + '" aria-hidden="true"></i>';
    return icon;
};

MyManager.getCurrencyIcon = function (currencyCode) {
    var icon = '<i class="fa ';
    switch(currencyCode) {
	case 'USD':
	case 'JMD':
	    icon = icon + 'fa-usd';
	    break;
	case 'EUR':
	    icon = icon + 'fa-eur';
	    break;
	case 'INR':
	    icon = icon + 'fa-inr';
	    break;
	case 'GBP':
	    icon = icon + 'fa-gbp';
	    break;
	case 'JPY':
	    icon = icon + 'fa-jpy';
	    break;
	case 'RUB':
	    icon = icon + 'fa-rub';
	    break;
	case 'ILS':
	    icon = icon + 'fa-ils';
	    break;
	case 'TRY':
	    icon = icon + 'fa-try';
	    break;
	case 'KPW':
	case 'KRW':
	    icon = icon + 'fa-won';
	    break;
	case 'OTH':
	    icon = icon + 'fa-money';
	    break;
	case 'BTC':
	    icon = icon + 'fa-btc';
	    break;
    }
    if(currencyCode === 'ZAR') return 'R';
    icon = icon + '" aria-hidden="true"></i>';
    return icon;
};

MyManager.getPlans = function() {
	/*
	 * $.ajax({ type : "POST", url : "", complete : function(plans) { var
	 * planText = plans.responseText; if (planText === 'failed') return null;
	 * else { return $.parseJSON(planText); } } });
	 */
	var planObj = [];
	planObj.push({"planId" : "1","planName" : "Free","price" : "0.00"});
	planObj.push({"planId" : "2","planName" : "Premium","price" : "2.49"});
	planObj.push({"planId" : "3","planName" : "Business","price" :"19.99"});
	// return null;
	return planObj;
};

MyManager.getDiscount = function(term, selectedPlanId) {
	var discount = "0.00";
	/*
	 * $.ajax({ type : "POST", url : "", complete : function(discounts) { var
	 * discountText = discounts.responseText; if (discountText === 'failed')
	 * return "0"; else {
	 */
	var discountText = [];
	discountText.push({
		"planId" : "1",
		"discountTermLimit" : "99",
		"discountPercentage" : "0.00"
	});
	discountText.push({
		"planId" : "2",
		"discountTermLimit" : "14",
		"discountPercentage" : "4.56"
	});
	discountText.push({
		"planId" : "2",
		"discountTermLimit" : "23",
		"discountPercentage" : "8.99"
	});
	discountText.push({
		"planId" : "2",
		"discountTermLimit" : "35",
		"discountPercentage" : "14.36"
	});
	discountText.push({
		"planId" : "3",
		"discountTermLimit" : "25",
		"discountPercentage" : "7.45"
	});
	discountText.push({
		"planId" : "3",
		"discountTermLimit" : "35",
		"discountPercentage" : "9.36"
	});
	discountText.push({
		"planId" : "3",
		"discountTermLimit" : "45",
		"discountPercentage" : "12.36"
	});
	discountText.push({
		"planId" : "3",
		"discountTermLimit" : "55",
		"discountPercentage" : "15.16"
	});
	discountText.push({
		"planId" : "3",
		"discountTermLimit" : "60",
		"discountPercentage" : "17.99"
	});
	$.each(discountText, function(i, item) {
		if (item.planId === selectedPlanId) {
			if (parseInt(term) > item.discountTermLimit) {
				discount = item.discountPercentage;
			}
		}
	});
	return discount;
	/*
	 * } } });
	 */
};