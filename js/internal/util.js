var MyManager = MyManager || {};

String.prototype.isEmpty = function() {
    return (this.length === 0 || !this.trim());
};

MyManager.isTouchDevice = function (){
    return true === ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
};

MyManager.isValidEmail = function (email) {
    var emailReg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return emailReg.test(email);
};

MyManager.isValidNumberWithDecimal = function (number,noOfDecimals) {
	var numReg;
	if(noOfDecimals === undefined)
    	numReg = /^[0-9]+\.?[0-9]{0,2}$/;
    else {
    	var regexText = '^[0-9]+\\.?[0-9]{0,'+noOfDecimals+'}$';
    	numReg = new RegExp(regexText);
    }
    return numReg.test(number);
};

MyManager.isValidNumber = function (number, limit) {
	var numReg;
	if(limit === undefined)
		numReg = /^[0-9]+$/;
    else {
    	var regexText = "^[0-9]{"+limit+"}$";
    	numReg = new RegExp(regexText);
    }
    return numReg.test(number);
};

MyManager.isPasswordFormatMatch = function (password) {
    var passwordReg = /^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#$%^.,><&*-_+=]).*$/;
    return passwordReg.test(password);
};

MyManager.limitNumberFieldLength = function (numberField, length) {
    if ($.trim($(number).val()).length >= length) {
        $(number).val($(number).val().substring(0, length - 1));
    }
};

MyManager.showAlertInBox = function (alertElementBox, isShowable, alertMessage, alertType) {
    if (typeof alertMessage !== 'undefined') {
        var markup = '<div class="alert alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button>' + alertMessage + '</div>';
    }
    alertElementBox.find('.alert').remove();
    if (isShowable) {
        alertElementBox.append(markup);
    } 
    if (typeof alertType !== 'undefined') {
        if (alertType === 'warning') {
            alertElementBox.find('.alert').addClass('alert-warning');
        }
        if (alertType === 'error') {
            alertElementBox.find('.alert').addClass('alert-danger');
        }
        if (alertType === 'success') {
            alertElementBox.find('.alert').addClass('alert-success');
        }
        if (alertType === 'info') {
            alertElementBox.find('.alert').addClass('alert-info');
        }
    } else {
        alertElementBox.find('.alert').removeClass();
    }
};

MyManager.showProgressBarModalWindow = function (heading, progressMsg, isLongMessage, operationType) {
    $('.progress-modal-title').text(heading);
    $('.progress-modal-body').html(progressMsg);
    if (typeof isLongMessage !== 'undefined') {
        if (isLongMessage) {
            $('#showProgressModalDialog').find('.modal-content').css('width', '130%');
            $('#showProgressModalDialog').find('.modal-body').css('height', '100%');
        }
    }
    $('#showProgressModalDialog').find('.progress-bar').removeClass('progress-bar-danger progress-bar-success progress-bar-warning progress-bar-info');
    if (typeof operationType !== 'undefined' && operationType.toString().trim() !== '') {
        operationType = operationType.toLowerCase();
        if (operationType === 'read') {
            $('#showProgressModalDialog').find('.progress-bar').addClass('progress-bar-warning');
        } else if (operationType === 'delete') {
            $('#showProgressModalDialog').find('.progress-bar').addClass('progress-bar-danger');
        } else if (operationType === 'save') {
            $('#showProgressModalDialog').find('.progress-bar').addClass('progress-bar-info');
        } else {
            $('#showProgressModalDialog').find('.progress-bar').addClass('progress-bar-success');
        }
    } else {
        $('#showProgressModalDialog').find('.progress-bar').addClass('progress-bar-success');
    }
    $('#showProgressModalDialog').modal('show');
};

MyManager.hideProgressBarModalWindow = function () {
    $('#showProgressModalDialog').modal('hide');
};

MyManager.showSuccessModalWindows = function (heading, successMessage, afterCloseHandler, isLongMessage) {
    var columnClassSelect = 'col-md-6 col-md-offset-3';
    if (isLongMessage) {
        columnClassSelect = 'col-md-8 col-md-offset-2';
    }
    $.alert({
        title: heading.toString(),
        titleClass: 'success',
        confirmButton: 'Ok',
        autoClose: 'confirm|30000',
        animationBounce: 2.5,
        backgroundDismiss: false,
        confirmButtonClass: 'btn-info',
        content: successMessage.toString(),
        confirm: afterCloseHandler,
        columnClass: columnClassSelect
    });

};

MyManager.showErrorModalWindows = function (heading, errorMessage, afterCloseParam, isLongMessage) {
    var columnClassSelect = 'col-md-6 col-md-offset-3';
    if (isLongMessage) {
        columnClassSelect = 'col-md-8 col-md-offset-2';
    }
    $.alert({
        title: heading.toString(),
        titleClass: 'error',
        confirmButton: 'Ok',
        autoClose: 'confirm|30000',
        animationBounce: 2.5,
        backgroundDismiss: false,
        confirmButtonClass: 'btn-warning',
        content: errorMessage.toString(),
        confirm: afterCloseParam,
        columnClass: columnClassSelect
    });
};

MyManager.spinnerStartLoading = function (item, loadingText) {
    var targetElement = item.toString();
    console.log('item: '+item);
    console.log('item in another form: '+JSON.stringify($(targetElement)));
    $data = {
        autoCheck: 32,
        size: 32,
        bgColor: '#FFF',
        bgOpacity: '0.7',
        fontColor: '#000',
        title: '&nbsp;&nbsp;&nbsp;'+loadingText.toString(),
        isOnly: !$('#isOnly').is(':checked')
    };
    if (targetElement === 'body') {
        $.loader.open($data);
    }
    else {
        $(targetElement).loader($data);
    }
};

MyManager.spinnerStopLoading = function () {
    $.loader.close(true);
};

MyManager.sorter = function (json_array_to_sort, key_to_sort_by) {
    function sortByKey(a, b) {
        var x = a[key_to_sort_by];
        var y = b[key_to_sort_by];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    }
    json_array_to_sort.sort(sortByKey);
};
