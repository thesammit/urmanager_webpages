var MyManager = MyManager || {};
var MyManagerDashboard = MyManagerDashboard || {};

var selectiveMonths = [];
var bankBalanceLabels = [];
var bankBalanceData = [];
var bankBalanceDataSet = [];
var balanceDataSource;
var detailedEMIDataSource;
var dashBoardDataSource;
var expenseBudget = [];
var expenseData = [];
var incomeBudget = [];
var incomeData = [];
var expenseZoneData = [];
var creditCardData = {"credit":[], "debit":[]};
var investmentData = [];
var configData;

var MONTH = MyManager.MONTH;	
var FULLMONTH = MyManager.FULLMONTH;

$(function() {
	if(MyManager.isTouchDevice() === false) {
		$('[data-rel="tooltip"]').tooltip();
	}
	$('.datetimepicker').datetimepicker({
		format : MyManager.inputDateFormat,
		showClear : true,
		minDate : moment().subtract(150, 'y'),
		maxDate : moment()
	});
	/* Menu Toggle Script */
	if ($(window).width() < 768)
		$('.sidebar-brand').hide();
	else
		$('.sidebar-brand').show();
	if ($(window).width() < 425) {
		$('.panel-body').find('.amount').removeClass('h5').addClass('.h6');
		$('.panel-body').find('.hideInVerySmall').hide();
	} else {
		$('.panel-body').find('.amount').addClass('h5').removeClass('.h6');
		$('.panel-body').find('.hideInVerySmall').show();
	}
	MyManagerDashboard.getAllDataFromDatabase();
	configData = MyManager.getConfigData();
	populateDataSources();
	$('.currency').html(MyManager.getCurrencyIcon(configData.currencyIcon));
	/* Chart Settings Call */
});

var populateDataSources = function() {
	balanceDataSource = MyManagerDashboard.getBankBalanceDetails();
	detailedEMIDataSource = MyManagerDashboard.getEMIs();
	dashBoardDataSource = MyManagerDashboard.getDashBoardCollection();
};

var prepareBankBalanceChartData = function() {
	var allBankAccounts = configData.bankAccounts;
	var labels = [];
	var data = [];
	var colors =[MyManager.colors.DANGER,
	             MyManager.colors.SUCCESS,
	             MyManager.colors.INFO,
	             MyManager.colors.WARNING,
	             MyManager.colors.PRIMARY,
	             MyManager.colors.DEFAULT,
	             MyManager.colors.SKYBLUE,
	             MyManager.colors.YELLOW,
	             MyManager.colors.CHOCOLATE,
	             MyManager.colors.GREY];
	
	$.each(allBankAccounts, function(indx, account) {
		$.map(balanceDataSource, function(balanceData) {
			if (account === balanceData.accountNumber) {
				labels.push(balanceData.date);
				data.push(balanceData.balance);
			}
		});
		if(labels.length > bankBalanceLabels.length && bankBalanceLabels.length < 8) {
			bankBalanceLabels = [];
			for (index = 0; index < 8; index++) {
				if(undefined !== labels[index])
					bankBalanceLabels.push(labels[index]);
			}
		}
		for (index = 0; index < 8; index++) {
			if(undefined !== data[index])
				bankBalanceData.push(data[index]);
		}
		labels=[];
		data=[];
		bankBalanceData.reverse();
		var dataset = {
				label : 'A/c:**'+account.substring(account.length-4),
				data : bankBalanceData,
				fill : false,
				borderColor : MyManager.getColorOfSet(colors[indx], 1),
				backgroundColor : MyManager.getColorOfSet(colors[indx], .34),
				pointBorderColor : MyManager.getColorOfSet(colors[indx], .34),
				pointBackgroundColor : MyManager.getColorOfSet(colors[indx], .34),
				pointBorderWidth : 1
			};
		bankBalanceData = [];
		bankBalanceDataSet.push(dataset);
	});
	bankBalanceLabels.reverse();
	configBankBalance.data.labels = bankBalanceLabels;
	configBankBalance.data.datasets = bankBalanceDataSet;
};

var prepareChartData = function() {
	var monthSelector = MyManager.currentMonth;
	var yearSelector = MyManager.currentYear;
	
	for (noOfLables = 0 ; noOfLables < parseInt(configData.noOfMonthsOnChart, 10) ; noOfLables++ ) {
		if (monthSelector >= 0) {
			selectiveMonths.push(MONTH[monthSelector]+" "+yearSelector);
			
			$.each(dashBoardDataSource, function(index, item) {
				if(MONTH[monthSelector] === item.month && yearSelector.toString() === item.year) {
					expenseBudget.push(item.budgetExpense);
					incomeBudget.push(item.budgetIncome);
					return false;
				}
			});
			
			$.each(dashBoardDataSource, function(index, item) {
				if(MONTH[monthSelector] === item.month && yearSelector.toString() === item.year) {
					expenseData.push(item.expense);
					return false;
				}
			});
			
			$.each(dashBoardDataSource, function(index, item) {
				if(MONTH[monthSelector] === item.month && yearSelector.toString() === item.year) {
					incomeData.push(item.income);
					return false;
				}
			});
			
			$.each(dashBoardDataSource, function(index, item) {
				if(MONTH[monthSelector] === item.month && yearSelector.toString() === item.year) {
					creditCardData.credit.push(item.credit);
					creditCardData.debit.push(item.debit);
					return false;
				}
			});

			$.each(dashBoardDataSource, function(index, item) {
				if(MONTH[monthSelector] === item.month && yearSelector.toString() === item.year) {
					investmentData.push(item.investment);
					return false;
				}
			});
			if (noOfLables === 0) {
				$('#selectMonth').append($("<option></option>").val(monthSelector+'/'+yearSelector).text("Current Month"));
			} else if (noOfLables !== 0) {
				$('#selectMonth').append($("<option></option>").val(monthSelector+'/'+yearSelector).text(FULLMONTH[monthSelector]+' '+yearSelector));
			}
			monthSelector--;
		} else {
			monthSelector = 11;
			noOfLables--;
			yearSelector--;
		}
	}
	$.each(dashBoardDataSource, function(index, item) {
		if (MONTH[MyManager.currentMonth] === item.month && MyManager.currentYear.toString() === item.year) {
			$.each(MyManager.expenseGroups, function(indx, expenseType){
				$.map(item, function(itemVal, itemKey) {
					if(expenseType === itemKey)
						expenseZoneData.push(itemVal);					
				});
			});
		}		
	});
};

var getStatusText = function(statusCode, dueDate){
	var text = '';
	var daysFromNow = moment(dueDate,MyManager.dateFormat).fromNow();
	switch(statusCode) {
	case 0:
		text = 'Payment is Done';
		break;
	case 1:
		text = 'Payment is Pending '+daysFromNow;
		break;
	case 2:
		text = 'Payment date has passed '+daysFromNow;
		break;
	}
    return text;
};

var setupUserReports = function() {
	$('#monthDetails').text(FULLMONTH[MyManager.currentMonth]+' '+MyManager.currentYear);
	var totalEMI=0;
	$.each(detailedEMIDataSource, function(index, item) {
		if (MONTH[MyManager.currentMonth] === item.month && MyManager.currentYear.toString() === item.year) {
			var emiTemplate = $('#holderEMIDetails #emiTemplate').clone();
			emiTemplate.find('.emiServiceName').text(item.name).prop('title',item.name);
			emiTemplate.find('.emiServiceAmount').text(item.amount);
			var status = parseInt(item.status,10);
			emiTemplate.find('.emiStatus').html(MyManager.getStatusIcon(status)).prop('title',getStatusText(status,item.dueDate));
			$('#emiDetails').append(emiTemplate.html());
			totalEMI = totalEMI + parseFloat(item.amount,10);
		}
	});
	
	$('#totalMonthlyExpenseAmmount').text(parseFloat(dashBoardDataSource[0].expense,10).toFixed(2));
	$('#totalMonthlyIncomeAmmount').text(parseFloat(dashBoardDataSource[0].income,10).toFixed(2));
	$('#totalMonthlyCreditAmmount').text(parseFloat(dashBoardDataSource[0].debit,10).toFixed(2));
	$('.totalEMIAmmount').text(totalEMI.toFixed(2));
	
	$('.budgetExpensePlannedMonth').text(MONTH[MyManager.currentMonth]+' '+MyManager.currentYear);
	$('.budgetExpensePlannedAmount').text(parseFloat(dashBoardDataSource[0].budgetExpense,10).toFixed(2));
	$('.budgetExpenseActualAmount').text(parseFloat(dashBoardDataSource[0].expense,10).toFixed(2));
	var budgetExpenseBalanceAmount = parseFloat(dashBoardDataSource[0].budgetExpense,10)-parseFloat(dashBoardDataSource[0].expense,10);
	$('.budgetExpenseBalanceAmount').text(budgetExpenseBalanceAmount.toFixed(2));
	var budgetExpensePercentage = 100*parseFloat(dashBoardDataSource[0].expense,10)/parseFloat(dashBoardDataSource[0].budgetExpense,10);
	$('.budgetExpensePercentage').text(budgetExpensePercentage.toFixed(2));
	
	$('.creditLimitAmount').text(parseFloat(configData.creditCardLimit,10).toFixed(2));
	$('.creditCardDebitAmount').text(parseFloat(dashBoardDataSource[0].debit,10).toFixed(2));
	$('.creditCardCreditAmount').text(parseFloat(dashBoardDataSource[0].credit,10).toFixed(2));
	var creditCardBalanceAmount = parseFloat(configData.creditCardLimit,10) - parseFloat(dashBoardDataSource[0].debit,10) + parseFloat(dashBoardDataSource[0].credit,10);
	$('.creditCardBalanceAmount').text(creditCardBalanceAmount.toFixed(2));
	$('.creditCardAvailablePoints').text(dashBoardDataSource[0].points);
};

/* <Menu Toggle Script> */
$(document).on('click', '#menu-toggle', function(e) {
	e.preventDefault();
	$("#wrapper").toggleClass("toggled");
	if ($('.sidebar-brand').is(':visible') && !$("#wrapper").hasClass('toggled')) {
		$('.sidebar-brand').hide();
	} else {
		$('.sidebar-brand').show();
	}
});

$(window).resize(function(e) {
	e.preventDefault();
	if ($(window).width() < 768)
		$('.sidebar-brand').hide();
	else
		$('.sidebar-brand').show();
	if ($("#wrapper").hasClass('toggled'))
		$("#wrapper").toggleClass("toggled");
	if ($(window).width() < 425) {
		$('.panel-body').find('.amount').removeClass('h5').addClass('.h6');
		$('.panel-body').find('.hideInVerySmall').hide();
	} else {
		$('.panel-body').find('.amount').addClass('h5').removeClass('.h6');
		$('.panel-body').find('.hideInVerySmall').show();
	}
});

/* Chart Expense configuration */
var configExpense = {
	type : 'line',
	data : {
		labels : selectiveMonths,
		datasets : [
				{
					label : "Budget",
					data : expenseBudget,
					fill : false,
					borderDash : [ 5, 3 ],
					borderColor : MyManager.getColorOfSet(MyManager.colors.CHOCOLATE, 1),
					backgroundColor : MyManager.getColorOfSet(MyManager.colors.WHITE, 1),
					pointBorderColor : MyManager.getColorOfSet(MyManager.colors.CHOCOLATE, .34),
					pointBackgroundColor : MyManager.getColorOfSet(MyManager.colors.WHITE, 1),
					pointBorderWidth : 1,
				},
				{
					label : "Expenditure",
					data : expenseData,
					fill : true,
					borderColor : MyManager.getColorOfSet(MyManager.colors.DANGER, 1),
					backgroundColor : MyManager.getColorOfSet(MyManager.colors.DANGER, .34),
					pointBorderColor : MyManager.getColorOfSet(MyManager.colors.DANGER, .34),
					pointBackgroundColor : MyManager.getColorOfSet(MyManager.colors.DANGER, .34),
					pointBorderWidth : 1,
				} ]
	},
	options : {
		responsive : true,
		maintainAspectRatio : false,
		legend : {
			position : 'bottom',
			labels : {
				boxWidth : 0
			}
		},
		hover : {
			mode : 'label'
		},
		scales : {
			xAxes : [ {
				display : true,
				scaleLabel : {
					display : false,
					labelString : 'Month'
				}
			} ],
			yAxes : [ {
				display : true,
				scaleLabel : {
					display : true,
					labelString : 'Expense'
				}
			} ]
		},
		title : {
			display : false,
			text : 'Expenses'
		}
	}
};

/* Chart Expense configuration */
var configIncome = {
	type : 'line',
	data : {
		labels : selectiveMonths,
		datasets : [
				{
					label : "Expected",
					data : incomeBudget,
					fill : false,
					borderDash : [ 5, 3 ],
					borderColor : MyManager.getColorOfSet(MyManager.colors.CHOCOLATE, 1),
					backgroundColor : MyManager.getColorOfSet(MyManager.colors.WHITE, 1),
					pointBorderColor : MyManager.getColorOfSet(MyManager.colors.CHOCOLATE, .34),
					pointBackgroundColor : MyManager.getColorOfSet(MyManager.colors.WHITE, 1),
					pointBorderWidth : 1,
				},
				{
					label : "Income",
					data : incomeData,
					fill : true,
					borderColor : MyManager.getColorOfSet(MyManager.colors.SUCCESS, 1),
					backgroundColor : MyManager.getColorOfSet(MyManager.colors.SUCCESS, .34),
					pointBorderColor : MyManager.getColorOfSet(MyManager.colors.SUCCESS, .34),
					pointBackgroundColor : MyManager.getColorOfSet(MyManager.colors.SUCCESS, .34),
					pointBorderWidth : 1,
				} ]
	},
	options : {
		responsive : true,
		maintainAspectRatio : false,
		legend : {
			position : 'bottom',
			labels : {
				boxWidth : 0
			}
		},
		hover : {
			mode : 'label'
		},
		scales : {
			xAxes : [ {
				display : true,
				scaleLabel : {
					display : false,
					labelString : 'Month'
				}
			} ],
			yAxes : [ {
				display : true,
				scaleLabel : {
					display : true,
					labelString : 'Income'
				}
			} ]
		},
		title : {
			display : false,
			text : 'Income'
		}
	}
};
/* Chart Expense Zone configuration */
var configExpenseZone = {
	type : 'pie',
	data : {
		labels : MyManager.expenseGroups,
		datasets : [ {
			data : expenseZoneData,
			backgroundColor : MyManagerDashboard.expenseGroupColors,
		} ],
	},
	options : {
		responsive : true,
		maintainAspectRatio : false,
		legend : {
			position : 'bottom',
			labels : {
				boxWidth : 10
			}
		}
	}
};
/* Chart Credit Card configuration */
var configCreditCard = {
	type : 'bar',
	data : {
		labels : selectiveMonths,
		datasets : [
				{
					label : 'Credits',
					backgroundColor : MyManager.getColorOfSet(MyManager.colors.SUCCESS, .54),
					hoverBackgroundColor : MyManager.getColorOfSet(MyManager.colors.SUCCESS, .85),
					borderColor : MyManager.getColorOfSet(MyManager.colors.SUCCESS, 1),
					borderWidth : 2,
					borderSkipped : 'bottom',
					data : creditCardData.credit,
				},
				{
					label : 'Debits',
					backgroundColor : MyManager.getColorOfSet(MyManager.colors.DANGER, .54),
					hoverBackgroundColor : MyManager.getColorOfSet(MyManager.colors.DANGER, .85),
					borderColor : MyManager.getColorOfSet(MyManager.colors.DANGER, 1),
					borderWidth : 2,
					borderSkipped : 'bottom',
					data : creditCardData.debit,
				} ]
	},
	options : {
		responsive : true,
		maintainAspectRatio : false,
		legend : {
			position : 'bottom',
			labels : {
				boxWidth : 10
			}
		},
		hover : {
			mode : 'label'
		},
		scales : {
			xAxes : [ {
				display : true,
				scaleLabel : {
					display : false,
					labelString : 'Month'
				}
			} ],
			yAxes : [ {
				display : true,
				scaleLabel : {
					display : true,
					labelString : 'Amount'
				}
			} ]
		},
		title : {
			display : false,
			text : 'Credit Card'
		}
	}
};

/* Chart Investment configuration */
var configInvestment = {
	type : 'bar',
	data : {
		labels : selectiveMonths,
		datasets : [
				{
					label : 'Invested',
					backgroundColor : MyManager.getColorOfSet(MyManager.colors.SUCCESS, .54),
					hoverBackgroundColor : MyManager.getColorOfSet(MyManager.colors.SUCCESS, .85),
					borderColor : MyManager.getColorOfSet(MyManager.colors.SUCCESS, 1),
					borderWidth : 2,
					borderSkipped : 'bottom',
					data : investmentData,
				}]
	},
	options : {
		responsive : true,
		maintainAspectRatio : false,
		legend : {
			position : 'bottom',
			labels : {
				boxWidth : 10
			}
		},
		hover : {
			mode : 'label'
		},
		scales : {
			xAxes : [ {
				display : true,
				scaleLabel : {
					display : false,
					labelString : 'Month'
				}
			} ],
			yAxes : [ {
				ticks: {
                    beginAtZero: true
                },
				display : true,
				scaleLabel : {
					display : true,
					labelString : 'Monthly Investment'
				}
			} ]
		},
		title : {
			display : false,
			text : 'Expenses'
		}
	}
};

/* Chart balance configuration */
var configBankBalance = {
	type : 'line',
	data : {
		labels : bankBalanceLabels,
		datasets : bankBalanceDataSet
	},
	options : {
		responsive : true,
		maintainAspectRatio : false,
		legend : {
			position : 'bottom',
			labels : {
				boxWidth : 0
			}
		},
		hover : {
			mode : 'label'
		},
		scales : {
			xAxes : [ {
				display : true,
				scaleLabel : {
					display : false,
					labelString : 'Month'
				}
			} ],
			yAxes : [ {
				display : true,
				scaleLabel : {
					display : true,
					labelString : 'Monthly Balance'
				}
			} ]
		},
		title : {
			display : false,
			text : 'Monthly Bank Balance'
		}
	}
};

var ctx;
var expenseLineChart;
var incomeLineChart;
var expenseZonePieChart;
var creditCardBarChart;
var InvestmentBarChart;
var bankBalanceLineChart;

window.onload = function() {
	prepareBankBalanceChartData();
	prepareChartData();
	setupUserReports();
	ctx = $('#expenseComparisonChart');
	expenseLineChart = new Chart(ctx, configExpense);
	ctx = $('#incomeComparisonChart');
	incomeLineChart = new Chart(ctx, configIncome);
	ctx = $('#expenseZonesChart');
	expenseZonePieChart = new Chart(ctx, configExpenseZone);
	ctx = $('#creditHistoryChart');
	creditCardBarChart = new Chart(ctx, configCreditCard);
	ctx = $('#investmentHistoryChart');
	InvestmentBarChart = new Chart(ctx, configInvestment);
	ctx = $('#bankBalanceHistoryChart');
	bankBalanceLineChart = new Chart(ctx, configBankBalance);
};

/* To Update with dynamic data */
$(document).on('change','#selectMonth',function() {
	var selectedValue = $(this).val().split("/");
	var thisMonth = selectedValue[0];
	var thisYear = selectedValue[1];
	console.log(MONTH[thisMonth]+": "+thisYear);
	expenseZoneData=[];
	$.each(dashBoardDataSource, function(index, item) {
		if(MONTH[thisMonth] === item.month && thisYear.toString() === item.year) {
			$.each(MyManager.expenseGroups, function(indx, expenseType){
				$.map(item, function(itemVal, itemKey) {
					if(expenseType === itemKey)
						expenseZoneData.push(itemVal);					
				});
			});
		}		
	});
	configExpenseZone.data.datasets[0].data = expenseZoneData;
	expenseZonePieChart.update();
});
