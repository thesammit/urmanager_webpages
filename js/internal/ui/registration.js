var MyManager = MyManager || {};

$(function() {
	if(MyManager.isTouchDevice()===false) {
		$('[data-rel="tooltip"]').tooltip();
	}
	$('#datetimepicker1').datetimepicker({
		format : MyManager.inputDateFormat,
		showClear : true,
		minDate : moment().subtract(150, 'y'),
		maxDate : moment()
	});
	$('#step2Tab').attr('href', "").addClass('disabled');
	$('#step3Tab').attr('href', "").addClass('disabled');

	MyManager.planObject = MyManager.getPlans();
	var blankArray = [];
	var elementHTML;
	if (null === MyManager.planObject) {
		elementHTML = '<p class="h3 text-center text-danger"><strong><i class="fa fa-warning"></i> 500: Internal Server error !!</strong></p>'
				+ '<p class="h6 text-center"><strong>Please continue your setup after logging in after the problem is resolved.</strong></p>';
		$('#planLists').append(elementHTML);
	} else if (0 === MyManager.planObject.length) {
		elementHTML = '<p class="h3 text-center text-primary"><i class="fa fa-info-circle"></i> No plans present !</p>'
				+ '<p class="h6 text-center"><strong>Please contact support to add plans and then Login to choose your plan and complete setup.</strong></p>';
		$('#planLists').append(elementHTML);
	} else {
		$.each(MyManager.planObject,function(i, item) {
							elementHTML = '<div class="col-sm-4 clear-left-padding"><div class="panel panel-primary" data-status="0" data-value="'
									+ item.planId
									+ '"><div class="panel-heading">'
									+ '<h3 class="panel-title"><span class="planName">'
									+ item.planName
									+ '</span> Plan</h3></div><div class="panel-body"><p>Panel content<br> more content <br> few more content</p>'
									+ '<p class="text-center"><i class="fa fa-usd"></i> <span class="h5 planAmount">'
									+ item.price
									+ '</span> per month</p><div class="text-center bottom-min-margin">'
									+ '<select class="plan-select-space termYear"><option>0</option><option>1</option><option>2</option><option>3</option><option>4</option>'
									+ '<option>5</option></select> Yr <select class="plan-select-space termMonth"><option>0</option><option selected>1</option><option>2</option><option>3</option><option>4</option>'
									+ '<option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option><option>11</option>'
									+ '</select> Mth</div><legend></legend><button type="button" class="btn btn-primary btn-block text-bold"><strong>Select</strong></button></div></div></div>';
			$('#planLists').append(elementHTML);
		});
	}
});

$(document).on('click', '#step1Tab', function(e) {
	var alertContainer = $('#step1Alert');
	var infoMessage = '<strong><i class="fa fa-info-circle"></i> info: </strong>';
	infoMessage += ' Your account has been opened with these data, You can change after completing your setup and signing in.';
	if (1 === $(this).data('value')) {
		MyManager.showAlertInBox(alertContainer, true, infoMessage, 'info');
	}
});

$(document).on('click', '#backToRegSteps', function(e) {
	$('#finalizeButton').prop('href', '');
});

$(document).on('click', '.panel-body button', function(e) {
	if (1 === $(this).parents('.panel').data('status')) {
		$(this).removeClass('btn-warning').addClass('btn-primary');
		$(this).parents('.panel').removeClass('panel-warning').addClass('panel-primary');
		$(this).parents('.panel').data('status', 0);
		$(this).find('strong').text("Select");
		$('#selectedProceed').prop('href', '');
	} else {
		var parentToThis = $(this).parents('.col-sm-12');
		parentToThis.find('.panel').removeClass('panel-warning').addClass('panel-primary');
		parentToThis.find('.panel').data('status', 0);
		parentToThis.find('button').removeClass('btn-warning').addClass('btn-primary');
		parentToThis.find('button strong').text("Select");
		$(this).addClass('btn-warning');
		$(this).parents('.panel').data('status', 1);
		$(this).parents('.panel').addClass('panel-warning');
		$(this).find('strong').text("Selected");
	}
});

$(document).on('click', '#selectedProceed', function(e) {
	e.preventDefault();
	flag = false;
	var selectedFinalPlan;
	$('#planSelection').find('.panel').each(function() {
		if (1 === $(this).data('status')) {
			flag = true;
			selectedFinalPlan = $(this);
			return false;
		}
	});
	var alertContainer = $('#step2Alert');
	if (flag) {
		MyManager.showAlertInBox(alertContainer, false);
		$('#step3Tab').attr('href', "#finish").removeClass('disabled');
		var itemID = selectedFinalPlan.data('value');
		var year = selectedFinalPlan.find('.termYear :selected').val();
		var month = selectedFinalPlan.find('.termMonth :selected').val();
		var term = parseInt(year) * 12 + parseInt(month);
		if (0 === term) {
			var message = "<strong>Error!</strong> Term period of usage must be more than or equal to 1 month.";
			MyManager.showAlertInBox(alertContainer, true, message, 'error');
		} else {
			$.each(MyManager.planObject, function(i, item) {
				if (itemID.toString() === item.planId) {
					$("#finalPlan").text(item.planName);
					$("#finalPlan").data('value', item.planId);
					$("#finalPlan").data('price', item.price);
					$("#finalPlan").data('term', term.toString());
					return false;
				}
			});
			$(this).prop('href', '#finish');
			$(this).tab('show');
		}
	} else {
		var message = "<strong>Error!</strong> Please select one of the plans to proceed.";
		MyManager.showAlertInBox(alertContainer, true, message, 'error');
	}
});

$(document).on('change', '#selectCurrency', function() {
	var currencyTextField = $('#currencyDetails');
	var currencyText = $(this).find("option:selected").attr("title") + " | ";
	currencyText = currencyText + MyManager.getCurrencyIcon($(this).find("option:selected").val());
	currencyTextField.html(currencyText);
});

$(document).on('focus blur', '#dateOfBirth', function() {
	var date = $(this).val();
	var showFullDate = $('#showFullDate');
	var age = $('#age');
	if (date !== '') {
		var enteredDate = moment(date, MyManager.inputDateFormat);
		var currentDate = moment();
		var dateOfBirth = enteredDate.format("dddd, MMMM DD, YYYY");
		var totalAge = currentDate.diff(enteredDate, 'years');
		showFullDate.text(dateOfBirth.toString() + " | ");
		age.text(totalAge.toString());
	} else {
		showFullDate.text('');
		age.text('0');
	}
});

MyManager.setupVerifyPage = function(regObject, finalObject) {
	var vat = MyManager.VAT * 100;
	var cess = MyManager.CESS * 100;
	var salesTax = 14;
	var totalPrice = finalObject.price * finalObject.term;
	var discountAmount = totalPrice * finalObject.discount / 100;
	var finalPrice = totalPrice - discountAmount;
	var salesTaxAmount = finalPrice * salesTax / 100;
	var vatAmount = finalPrice * MyManager.VAT;
	var cessAmount = vatAmount * MyManager.CESS;
	finalPrice = finalPrice + salesTaxAmount + vatAmount + cessAmount;
	MyManager.finalizeObject['discountAmount'] = discountAmount.toFixed(2);
	MyManager.finalizeObject['salesTaxAmount'] = salesTaxAmount.toFixed(2);
	MyManager.finalizeObject['vatAmount'] = vatAmount.toFixed(2);
	MyManager.finalizeObject['cessAmount'] = cessAmount.toFixed(2);
	MyManager.finalizeObject['finalPrice'] = finalPrice.toFixed(2);

	$('#finalCostPlan').text(finalObject.planDescription);
	$('#finalCostPrice').text(finalObject.price);
	$('#finalCostFinalPrice').text(totalPrice.toFixed(2));
	$('#finalCostDiscount').text(finalObject.discount);
	$('#finalCostDiscountAmount').text(discountAmount.toFixed(2));
	$('#finalCostSTAmount').text(salesTaxAmount.toFixed(2));
	$('#finalCostVATAmount').text(vatAmount.toFixed(2));
	$('#finalCostCESSAmount').text(cessAmount.toFixed(2));
	$('#finalCostTotal').text(finalPrice.toFixed(2));

	$('#finalAccountNumber').text(finalObject.accountNo);
	$('#finalMobile').text(regObject.mobile);
	$('#finalUserFullName').text(regObject.userLastName + ", " + regObject.userFirstName);
	$('#finalAge').text(finalObject.age + " Years");
	$('#finalEmail').text(regObject.emailId);
	$('#finalDateOfBirth').text(finalObject.dateOfBirth);
	$('#finalCurrency').text(finalObject.selectCurrency + " - " + finalObject.selectCurrencyDescription);
	if (finalObject.uniqueId === "")
		$('#finalUID').text("N/A");
	else
		$('#finalUID').text(finalObject.uniqueId);

	$('#finalAddrLine1').text(finalObject.aptno + ", " + finalObject.street + ", ");
	$('#finalAddrLine2').text(finalObject.landmark + ", " + finalObject.locality + ", ");
	$('#finalAddrLine3').text(finalObject.state + ", " + finalObject.city + ", ");
	$('#finalAddrLine4').text("PIN - " + finalObject.pin + ", " + finalObject.country);
	$('#verifyPlan').text(finalObject.planDescription);
	$('#finalPrice').text(finalPrice.toFixed(2));
	$('#finalTerm').text(finalObject.term);
	$('#finalPeriod').text(
			moment(finalObject.regStartDate, MyManager.dateFormat).format(MyManager.inputDateFormat) + " - "
					+ moment(finalObject.regEndDate, MyManager.dateFormat).format(MyManager.inputDateFormat));
};

$(document).on("click", '#submitToDB', function(e) {
	var result = MyManager.saveFinalData(MyManager.finalizeObject);
	if (result) {
		$('#successAccountNo').text(MyManager.finalizeObject.accountNo);
		$('#successEmail').text(MyManager.registrationObject.emailId);
		$(this).prop('href', '#resultMessageBox');
		$(this).tab('show');
	}
});

