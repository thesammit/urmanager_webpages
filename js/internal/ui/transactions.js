var MyManager = MyManager || {};
var MyManagerTransactions = MyManagerTransactions || {};
var configData={};
var beforeDisplayed=0;
var noOfStatementDisplayed = 10;
var noOfExpenseDisplayed = 10;
var noOfIncomeDisplayed = 10;
var loadMoreCount = 25;
var miniStatement = [];
var expenses = [];
var incomes = [];
var transactionType = {"INCOME":"Income","EXPENSE":"Expense"};

/* <Menu Toggle Script on resize window> */
$(window).resize(function(e) {
	e.preventDefault();
	if ($(window).width() < 768)
		$('.sidebar-brand').hide();
	else
		$('.sidebar-brand').show();
});

var populateTransactionTypeDropdowns = function() {
	$.map(transactionType, function(type){
		var option = $('<option/>').prop('value',type).text(type);
		$('#transactionType').append(option);
	});
};

var populateTransactionGroupDropdowns = function(){
	var selectedGroup=[];
	$('#transactionSubType').empty();
	if(transactionType.EXPENSE === $('#transactionType').val())
		selectedGroup = MyManager.expenseGroups;
	else
		selectedGroup = MyManager.incomeGroups;
	$.map(selectedGroup, function(type){
		var option = $('<option/>').prop('value',type).text(type);
		$('#transactionSubType').append(option);
	});
};

var populateCreditCardCheckBox = function() {
	if(transactionType.INCOME === $('#transactionType').val()) {
		$('#isTransactionCreditType').closest('.form-group').hide();
		$('#isTransactionCreditType').prop('checked',false);		
	}
	else{
		$('#isTransactionCreditType').closest('.form-group').show();
	}
};

 $(document).on('change','#transactionType', function(){
	 populateTransactionGroupDropdowns();
	 populateCreditCardCheckBox();
	 
 });

/* <Menu Toggle Script> */
$(document).on('click', '#menu-toggle', function(e) {
	e.preventDefault();
	$("#wrapper").toggleClass("toggled");
	if ($('.sidebar-brand').is(':visible') && !$("#wrapper").hasClass('toggled')) {
		$('.sidebar-brand').hide();
	} else {
		$('.sidebar-brand').show();
	}
});

$(document).on('click', '.showDetails', function(){
	var transactionData = JSON.parse($(this).find('td:last').text());
	$('#transactionReferenceNo').closest('.form-group').show();
	$('#transactionReferenceNo').val(transactionData.id);
	$('#transactionDate').val(transactionData.date);
	$('#transactionTime').val(transactionData.time);
	$('#transactionType').val(transactionData.type);
	populateTransactionGroupDropdowns();
	populateCreditCardCheckBox();
	$('#transactionSubType').val(transactionData.subtype);
	if("true" === transactionData.isCredit)
		$('#isTransactionCreditType').prop('checked',true);
	else
		$('#isTransactionCreditType').prop('checked',false);
	$('#transactionDescription').val(transactionData.desc);
	$('#transactionAmount').val(transactionData.amount);
    $('#transactionCreateButton').text('Update');
    $('#transactionDetails').find('.modal-title').text('Edit Transaction');
    checkDescription();
	$('#transactionDetails').modal();
});

$(document).on('click', '#addTransactionButton', function () {
	$('#transactionType').val(transactionType.INCOME);
	populateTransactionGroupDropdowns();
	populateCreditCardCheckBox();
	$("#transactionDate").val(moment().format(MyManager.inputDateFormat));
    $("#transactionTime").val(moment().format(MyManager.inputTimeFormat));
	$('#transactionReferenceNo').closest('.form-group').hide();
	checkDescription();
	$('#transactionDetails').modal();
});

$(document).on('click', '#transactionCreateButton', function () {
	 $.confirm({
        title: 'Confirm',
        content: 'Your data will be saved, Do want to Proceed?',
        confirmButton: 'Yes',
        cancelButton: 'No',
        animationBounce: 2.5,
        backgroundDismiss: false,
        confirmButtonClass: 'btn-primary',
        cancelButtonClass: 'btn-danger',
        confirm: function () {
        	var message = MyManagerTransactions.onClickTransactionCreateButton();
        	if(message.isEmpty()) {
        		MyManagerTransactions.saveData(MyManager.transactionObject);
        		resetModalAndClose();
        	} else{
        		$.dialog({
        		    title: 'Error',
        		    content: message,
        		});
        	}
        }
    });
});

$(document).on('keyup', '#transactionDescription', function () {
	checkDescription();
});

var checkDescription = function() {
	var transactionDescription = $('#transactionDescription');
	var textLength = transactionDescription.val().length;
	var noOfChars = $('#noOfChars');
	var transactionDescriptionStatus = $('#transactionDescriptionStatus');
	var diff = 250 - textLength;
	noOfChars.text(Math.abs(diff));
	if(diff < 0) {
		noOfChars.parent().addClass('text-danger');
		transactionDescriptionStatus.text('exceeded');
	} else {
		noOfChars.parent().removeClass('text-danger');
		transactionDescriptionStatus.text('remaining');
	}
};

var resetModalAndClose = function(){
	$("#transactionReferenceNo").val('');
    $("#transactionDate").val('');
    $("#transactionTime").val('');
    $("#transactionDescription").val('');
    $("#transactionAmount").val('');
    $('#transactionDetails').find('.form-group').removeClass('has-error');
    $('#transactionCreateButton').text('Create');
    $('#transactionDetails').find('.modal-title').text('Add Transaction');
    $('#transactionDetails').modal('hide');
};

$(document).on('click', '#transactionCancelButton, .modal-header .close', function () {
    $.confirm({
        title: 'Cancel',
        content: 'No data will be saved, Do want to Cancel?',
        confirmButton: 'Yes',
        cancelButton: 'No',
        animationBounce: 2.5,
        backgroundDismiss: false,
        confirmButtonClass: 'btn-primary',
        cancelButtonClass: 'btn-danger',
        confirm: function () {
        	resetModalAndClose();
        }
    });
});

MyManagerTransactions.transactionSetup = function() {
	miniStatement = MyManagerTransactions.getTransactions(noOfStatementDisplayed);
	noOfStatementDisplayed = loadButtonDispay($('#loadMoreTransaction'), miniStatement.length, noOfStatementDisplayed);
	MyManagerTransactions.appendToTable($('#miniStatementTable'), miniStatement, false, beforeDisplayed);
	
	expenses = MyManagerTransactions.getTransactionByTypes(noOfExpenseDisplayed, transactionType.EXPENSE);
	noOfExpenseDisplayed = loadButtonDispay($('#loadMoreExpense'), expenses.length, noOfExpenseDisplayed);
	MyManagerTransactions.appendToTable($('#expensesTable'), expenses, true, beforeDisplayed);
	
	incomes = MyManagerTransactions.getTransactionByTypes(noOfIncomeDisplayed, transactionType.INCOME);
	noOfIncomeDisplayed = loadButtonDispay($('#loadMoreIncome'), incomes.length, noOfIncomeDisplayed);
	MyManagerTransactions.appendToTable($('#incomesTable'), incomes, true, beforeDisplayed);
};

var setupCalendar = function() {
	$('#datetimePickerTransactionDate').datetimepicker({
		format : MyManager.inputDateFormat,
		showClear : true,
		minDate : moment().subtract(1, 'y'),
		maxDate : moment()
	});
	$('#datetimePickerTransactionTime').datetimepicker({
		format : MyManager.inputTimeFormat,
		showClear : true,
	});	
};

$(function () {
	if(MyManager.isTouchDevice() === false) {
		$('[data-rel="tooltip"]').tooltip();
	}
	setupCalendar();
	MyManagerTransactions.fetchAllTransactions();
	MyManagerTransactions.transactionSetup();
	/* Menu Toggle Script on ready*/
	if ($(window).width() < 768)
		$('.sidebar-brand').hide();
	else
		$('.sidebar-brand').show();
	configData = MyManager.getConfigData();
	populateTransactionTypeDropdowns();
	$('.currency').html(MyManager.getCurrencyIcon(configData.currencyIcon));

    $.tablesorter.themes.bootstrap = {
        table: 'table table-striped table-hover',
        caption: 'caption',
        header: '', // give the header a gradient background (theme.bootstrap_2.css)
        sortNone: '',
        sortAsc: '',
        sortDesc: '',
        active: '', // applied when column is sorted
        hover: '', // custom css required - a defined bootstrap style may not override other classes
        icons: '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
        iconSortNone: 'fa fa-sort fa-2x', // class name added to icon when column is not sorted
        iconSortAsc: 'fa fa-sort-asc fa-2x', // class name added to icon when column has ascending sort
        iconSortDesc: 'fa fa-sort-desc fa-2x', // class name added to icon when column has descending sort
        filterRow: '', // filter row class; use widgetOptions.filter_cssFilter for the input/select element
        footerRow: '',
        footerCells: '',
        even: '', // even row zebra striping
        odd: ''  // odd row zebra striping
    };

    //Table of Mini statement
    $("#miniStatementTable").tablesorter({
        theme: "bootstrap",
        widthFixed: false,
        headerTemplate: '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!
        widgets: ["uitheme", "filter", "zebra"],
        widgetOptions: {
            filter_ignoreCase: true,
            filter_reset: ".reset",
            filter_cssFilter: "form-control filter-box input-sm",
            zebra: ["even", "odd"],
            filter_functions : {
              4 : {
                  "< 1000" : function(e, n, f, i, $r, c, data) { return n < 1000; },
                  "1000 - 5000" : function(e, n, f, i, $r, c, data) { return n >= 1000 && n <=5000; },
                  "5000 - 10000" : function(e, n, f, i, $r, c, data) { return n >= 5000 && n <=10000; },
                  "10000 - 50000" : function(e, n, f, i, $r, c, data) { return n >= 10000 && n <=50000; },
                  "50000 - 100000" : function(e, n, f, i, $r, c, data) { return n >= 50000 && n <=100000; },
                  "> 100000" : function(e, n, f, i, $r, c, data) { return n > 100000; }
              	}
            }
        }
    })
    .tablesorterPager({
        container: $(".pagerMiniStatement"),
        cssGoto: ".pagenum",
        removeRows: false,
        output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'
    });
    
    //Table of Expenses
    $("#expensesTable").tablesorter({
        theme: "bootstrap",
        widthFixed: false,
        headerTemplate: '{content} {icon}',
        widgets: ["uitheme", "filter", "zebra"],
        widgetOptions: {
            filter_ignoreCase: true,
            filter_reset: ".reset",
            filter_cssFilter: "form-control filter-box input-sm",
            zebra: ["even", "odd"],
            filter_functions : {
                4 : {
                    "< 1000" : function(e, n, f, i, $r, c, data) { return n < 1000; },
                    "1000 - 5000" : function(e, n, f, i, $r, c, data) { return n >= 1000 && n <=5000; },
                    "5000 - 10000" : function(e, n, f, i, $r, c, data) { return n >= 5000 && n <=10000; },
                    "10000 - 50000" : function(e, n, f, i, $r, c, data) { return n >= 10000 && n <=50000; },
                    "50000 - 100000" : function(e, n, f, i, $r, c, data) { return n >= 50000 && n <=100000; },
                    "> 100000" : function(e, n, f, i, $r, c, data) { return n > 100000; }
                }
            }
        }
    })
    .tablesorterPager({
        container: $(".pagerExpense"),
        cssGoto: ".pagenum",
        removeRows: false,
        output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'
    })
    
    //Table of Incomes
    $("#incomesTable").tablesorter({
        theme: "bootstrap",
        widthFixed: false,
        headerTemplate: '{content} {icon}',
        widgets: ["uitheme", "filter", "zebra"],
        widgetOptions: {
            filter_ignoreCase: true,
            filter_reset: ".reset",
            filter_cssFilter: "form-control filter-box input-sm",
            zebra: ["even", "odd"],
            filter_functions : {
                4 : {
                  "< 1000" : function(e, n, f, i, $r, c, data) { return n < 1000; },
                  "1000 - 5000" : function(e, n, f, i, $r, c, data) { return n >= 1000 && n <=5000; },
                  "5000 - 10000" : function(e, n, f, i, $r, c, data) { return n >= 5000 && n <=10000; },
                  "10000 - 50000" : function(e, n, f, i, $r, c, data) { return n >= 10000 && n <=50000; },
                  "50000 - 100000" : function(e, n, f, i, $r, c, data) { return n >= 50000 && n <=100000; },
                  "> 100000" : function(e, n, f, i, $r, c, data) { return n > 100000; }
                }
            }
        }
    })
    .tablesorterPager({
        container: $(".pagerIncome"),
        cssGoto: ".pagenum",
        removeRows: false,
        output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'
    });
});

$(document).on('click','.loadMore',function() {
	var thisButton = $(this);
	var listOfTransactions = [];
	var mneTransaction = thisButton.data('item');
	switch(mneTransaction){
	case 0:
		listOfTransactions = MyManagerTransactions.getTransactions(noOfStatementDisplayed, loadMoreCount);
		beforeDisplayed = noOfStatementDisplayed;
		MyManagerTransactions.appendToTable($('#miniStatementTable'), listOfTransactions, false, beforeDisplayed);
		$.merge(miniStatement,listOfTransactions);
		noOfStatementDisplayed = loadButtonDispay(thisButton, miniStatement.length, (noOfStatementDisplayed+loadMoreCount));
		break;
	case 1:
		listOfTransactions = MyManagerTransactions.getTransactionByTypes(noOfExpenseDisplayed, transactionType.EXPENSE, loadMoreCount)
		beforeDisplayed = noOfExpenseDisplayed;
		MyManagerTransactions.appendToTable($('#expensesTable'), listOfTransactions, true, beforeDisplayed);
		$.merge(expenses,listOfTransactions);
		noOfExpenseDisplayed = loadButtonDispay(thisButton, expenses.length, (noOfExpenseDisplayed+loadMoreCount));
		break;
	case 3:
		listOfTransactions = MyManagerTransactions.getTransactionByTypes(noOfIncomeDisplayed, transactionType.EXPENSE, loadMoreCount)
		beforeDisplayed = noOfIncomeDisplayed;
		MyManagerTransactions.appendToTable($('#incomesTable'), listOfTransactions, true, beforeDisplayed);
		$.merge(incomes,listOfTransactions);
		noOfIncomeDisplayed = loadButtonDispay(thisButton, incomes.length, (noOfIncomeDisplayed+loadMoreCount));
		break;
	}
});

var loadButtonDispay = function(loadButton, listSize, maxListSize) {
	var noOfTransactionDisplayed = 0;
	if(maxListSize > listSize){
		noOfTransactionDisplayed = listSize;
		loadButton.prop('disabled','true');
	} else {
		noOfTransactionDisplayed = maxListSize;
	}
	return noOfTransactionDisplayed;
};

MyManagerTransactions.appendToTable = function(elementToAppend, listOfTransactions, isOfType, beforeDisplayed) {
	var count=beforeDisplayed;
	$.map(listOfTransactions, function(aTransaction){
		count++;
		var transactionTypeClass="";
		var td1 = $('<td/>').html(count);
		var td2 = $('<td/>').attr('title', aTransaction.date+" "+aTransaction.time).html(aTransaction.date+' <span class="small-text">'+aTransaction.time+'</span>');
		var td3;
		if(isOfType){td3 = $('<td/>').attr('title',aTransaction.subtype).html(aTransaction.subtype);}
		else{td3 = $('<td/>').attr('title',aTransaction.type).html(aTransaction.type);}
		
		var td4 = $('<td/>').attr('title',aTransaction.desc).html(aTransaction.desc);		
		
		if(aTransaction.type === transactionType.EXPENSE){transactionTypeClass="text-danger";}
		else if(aTransaction.type === transactionType.INCOME){transactionTypeClass="text-success";}
		
		var currency = $('<span/>').addClass('currency');
		var td5 = $('<td/>').attr('title',aTransaction.amount).addClass(transactionTypeClass);
		td5.append(currency);
		td5.append(aTransaction.amount);
		
		var td6 = $('<td/>').attr('hidden',true);
		td6.html(JSON.stringify(aTransaction));
		
		var tr = $('<tr/>').data('id',aTransaction.id).addClass('showDetails');
		tr.append(td1);
		tr.append(td2);
		tr.append(td3);
		tr.append(td4);
		tr.append(td5);
		tr.append(td6);
		
		elementToAppend.find('tbody').append(tr);
		var resort = true;
		elementToAppend.trigger("update", [resort]);
		$('.currency').html(MyManager.getCurrencyIcon(configData.currencyIcon));
	});
};
